/** @format */

import React, { Component, PropTypes } from 'react';
import ReactNative, {
  requireNativeComponent,
  UIManager,
  StyleSheet,
} from 'react-native';
import {connect} from "react-redux";
import {setGalleryMode} from "../../app/actions/gallery";

const GalleryAnd = requireNativeComponent('GalleryAndroid', GalleryAndroid, {});

export class GalleryAndroid extends Component {

  exitSelectionMode() {
    UIManager.dispatchViewManagerCommand(
      this.getNodeHandle(),
      UIManager.GalleryAndroid.Commands.hide,
      null
    );
  }

  enterSelectionMode() {
    UIManager.dispatchViewManagerCommand(
      this.getNodeHandle(),
      UIManager.GalleryAndroid.Commands.show,
      null
    );
  }

  getNodeHandle() {
    return ReactNative.findNodeHandle(this.refs.gallery);
  }

  render() {
    return (
      <GalleryAnd
        {...this.props}
        ref="gallery"
        style={[styles.gallery, this.props.style]}
      />
    );
  }

}

const styles = StyleSheet.create({
  gallery: {
    flex: 1,
  }
});



export default GalleryAndroid;

