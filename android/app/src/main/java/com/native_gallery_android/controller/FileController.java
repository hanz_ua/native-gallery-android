package com.native_gallery_android.controller;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.native_gallery_android.photo.Photo;
import com.native_gallery_android.util.TypeMedia;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class FileController {

  public static List<Photo> getAllMedia(Context activity) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
      != PackageManager.PERMISSION_GRANTED) {
      return null;
    }
    List<Photo> photos = new ArrayList<>();
    File directory = new File(Objects.requireNonNull(activity.getExternalFilesDir(Environment.DIRECTORY_DCIM)).getPath() + "/.media");
    File[] files = directory.listFiles();
    if (files != null) {
      for (File file : files) {
        if (file.getName().contains(".jpg"))
          photos.add(new Photo(Objects.requireNonNull(activity.getExternalFilesDir(Environment.DIRECTORY_DCIM)).getPath() + "/.media/" + file.getName(), TypeMedia.PHOTO, false));
        else if(getFolderSize(file) > 0)
          photos.add(new Photo(Objects.requireNonNull(activity.getExternalFilesDir(Environment.DIRECTORY_DCIM)).getPath() + "/.media/" + file.getName(), TypeMedia.VIDEO, false));
      }
    } else {
      return null;
    }
    Collections.sort(photos);
    return photos;
  }

  private static long getFolderSize(File file) {
    long size = 0;
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        size += getFolderSize(child);
      }
    } else {
      size = file.length();
    }
    return size;
  }

  public static void deleteFiles(Map<Photo, View> map) {
    for (Photo photo : map.keySet()) {
      File file = new File(photo.getPath());
      file.delete();
    }
  }
}
