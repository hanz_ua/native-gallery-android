package com.native_gallery_android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

import com.crust87.texturevideoview.widget.TextureVideoView;

public class AutoFitVideo extends TextureVideoView {
  public AutoFitVideo(Context context) {
    super(context);
  }

  public AutoFitVideo(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public AutoFitVideo(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, widthMeasureSpec);
  }

}
