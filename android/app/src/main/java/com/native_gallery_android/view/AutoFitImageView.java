package com.native_gallery_android.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class AutoFitImageView extends android.support.v7.widget.AppCompatImageView {


  public AutoFitImageView(Context context) {
    super(context);
  }

  public AutoFitImageView(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public AutoFitImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }


  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, widthMeasureSpec);
  }


}
