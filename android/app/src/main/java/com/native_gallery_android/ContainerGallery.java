package com.native_gallery_android;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crust87.texturevideoview.widget.TextureVideoView;
import com.native_gallery_android.adapter.core.interfaces.OnItemCancelClickListener;
import com.native_gallery_android.adapter.photo.PhotoAdapter;
import com.native_gallery_android.controller.FileController;
import com.native_gallery_android.dialog.AlertDeleteFileDialog;
import com.native_gallery_android.photo.Photo;
import com.native_gallery_android.util.TypeMedia;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.native_gallery_android.util.UtilString.ITEM_IMAGE;
import static com.native_gallery_android.util.UtilString.PATH_PROVIDER;
import static com.native_gallery_android.util.UtilString.SELECT_ITEM;

public class ContainerGallery extends RelativeLayout {
  private ImageView image;
  private RecyclerView recyclerView;
  private TextureVideoView video;
  private PhotoAdapter photoAdapter;
  private View navigator;
  private TextView noting;
  private RelativeLayout container;
  private GalleryManager galleryManager;


  public ContainerGallery(final Context context, GalleryManager galleryManager) {
    super(context);
    this.galleryManager = galleryManager;
    View view = LayoutInflater.from(context).inflate(R.layout.gallery, null);
    image = view.findViewById(R.id.image);
    recyclerView = view.findViewById(R.id.recycler_view);
    video = view.findViewById(R.id.video);
    View trash = view.findViewById(R.id.trash);
    View share = view.findViewById(R.id.share);
    noting = view.findViewById(R.id.noting);
    container = view.findViewById(R.id.container);
    share.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        if (photoAdapter.photoViewMap.size() <= 0) {
          Toast.makeText(context, SELECT_ITEM, Toast.LENGTH_SHORT).show();
          return;
        }
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        sharingIntent.setType(ITEM_IMAGE);
        ArrayList<Uri> files = new ArrayList<>();
        for (Photo photo : photoAdapter.photoViewMap.keySet()) {
          Uri uri = FileProvider.getUriForFile(context, PATH_PROVIDER, new File(photo.getPath()));
          files.add(uri);
        }
        sharingIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
        context.startActivity(Intent.createChooser(sharingIntent, "Share Image:"));
      }
    });
    trash.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        if (photoAdapter.photoViewMap.size() > 0) {
          AlertDeleteFileDialog.showAlertDialog(context, photoAdapter.photoViewMap, new AlertDeleteFileDialog.DeleteItems() {
            @Override
            public void onSuccess() {
              hide();
              photoAdapter.setItems(setDefaultImage());
              recyclerView.scrollBy(0, 1);
            }
          });
        } else {
          Toast.makeText(context, SELECT_ITEM, Toast.LENGTH_SHORT).show();
        }
      }
    });
    navigator = view.findViewById(R.id.navigator);
    video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
      @Override
      public void onPrepared(MediaPlayer mp) {
        mp.setLooping(true);
      }
    });
    initAdapter();
    addView(view);
  }

  public List<Photo> setDefaultImage() {
    List<Photo> photos = FileController.getAllMedia(getContext());
    if (photos != null && (photos.size() > 0)) {
      if (photos.get(0).getType() == TypeMedia.PHOTO) {
        Glide.with(getContext()).load(new File(photos.get(0).getPath())).into(image);
      } else {
        initVideo(photos.get(0));
      }
    } else {
      Glide.with(getContext()).load(new File("")).into(image);
      noting.setVisibility(VISIBLE);
    }
    return photos;
  }

  public ContainerGallery(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ContainerGallery(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public ContainerGallery(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  private void initAdapter() {
    photoAdapter = new PhotoAdapter();
    photoAdapter.setClickListener(new OnItemCancelClickListener<Photo>() {

      @Override
      public void onItemClickPhoto(Photo photo, int position) {
        image.setVisibility(VISIBLE);
        video.setVisibility(INVISIBLE);
        Glide.with(getContext()).load(new File(photo.getPath())).into(image);
      }

      @Override
      public void onItemClickVideo(Photo photo, int position) {
        initVideo(photo);
      }

      @Override
      public void onItemClickCheck(Photo photo, int position) {

      }
    });
    photoAdapter.setItems(setDefaultImage());
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));
    recyclerView.setAdapter(photoAdapter);
  }

  private boolean anim;

  public void show() {
    if (anim) {
      return;
    } else {
      anim = true;
    }
    galleryManager.onShow();
    photoAdapter.setSelected(true);
    navigator.setVisibility(VISIBLE);
    navigator.setY(navigator.getY() + dpToPx(40));
    AnimatorSet decSet2 = new AnimatorSet();
    decSet2.playTogether(
      ObjectAnimator.ofFloat(navigator, "y", navigator.getY() - dpToPx(40)),
      ObjectAnimator.ofFloat(container, "y", container.getY() - dpToPx(40)),
      ObjectAnimator.ofFloat(image, "y", image.getY() - dpToPx(40)),
      ObjectAnimator.ofFloat(video, "y", video.getY() - dpToPx(40))
    );
    decSet2.addListener(new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animator) {

      }

      @Override
      public void onAnimationEnd(Animator animator) {
        anim = false;
      }

      @Override
      public void onAnimationCancel(Animator animator) {

      }

      @Override
      public void onAnimationRepeat(Animator animator) {

      }
    });
    decSet2.start();
  }

  public static int dpToPx(int dp) {
    return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
  }

  public void hide() {
    if (anim) {
      return;
    } else {
      anim = true;
    }
    galleryManager.onClose();
    photoAdapter.setSelected(false);
    AnimatorSet decSet2 = new AnimatorSet();
    decSet2.playTogether(
      ObjectAnimator.ofFloat(navigator, "y", navigator.getY() + dpToPx(40)),
      ObjectAnimator.ofFloat(container, "y", container.getY() + dpToPx(40)),
      ObjectAnimator.ofFloat(image, "y", image.getY() + dpToPx(40)),
      ObjectAnimator.ofFloat(video, "y", video.getY() + dpToPx(40))
    );
    decSet2.addListener(new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animator) {

      }

      @Override
      public void onAnimationEnd(Animator animator) {
        navigator.setY(navigator.getY() - dpToPx(40));
        navigator.setVisibility(GONE);
        anim = false;
      }

      @Override
      public void onAnimationCancel(Animator animator) {

      }

      @Override
      public void onAnimationRepeat(Animator animator) {

      }
    });
    decSet2.start();

  }

  private void initVideo(Photo photo) {
    image.setVisibility(INVISIBLE);
    video.setVisibility(VISIBLE);
    video.setVideoURI(Uri.parse(photo.getPath()));
    video.requestFocus();
    video.start();
  }
}
