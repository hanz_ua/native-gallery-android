package com.native_gallery_android.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import com.native_gallery_android.R;
import com.native_gallery_android.controller.FileController;
import com.native_gallery_android.photo.Photo;

import java.util.Map;

public class AlertDeleteFileDialog {

  public interface DeleteItems{
    public void onSuccess();
  }


  public static void showAlertDialog(final Context activity, final Map<Photo, View> map, final DeleteItems deleteItems) {
    AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogLightStyle);
    builder
      .setMessage("Delete items?")
      .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          FileController.deleteFiles(map);
          Toast.makeText(activity, "Items delete", Toast.LENGTH_SHORT).show();
          dialogInterface.cancel();
          deleteItems.onSuccess();
        }
      })
      .setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          dialogInterface.cancel();
        }
      });
    builder.show();
  }
}
