package com.native_gallery_android.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;

import com.native_gallery_android.R;

public class AlertMessageErrorDialog {

  public static void showAlertDialog(Context activity) {
    AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogLightStyle);
    builder
      .setMessage("You can select only up to 4 media files at once ")
      .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          dialogInterface.cancel();
        }
      });
    builder.show();
  }
}
