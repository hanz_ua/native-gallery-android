package com.native_gallery_android.adapter.core;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.native_gallery_android.adapter.core.interfaces.OnItemClickListener;

public abstract class BaseViewHolder<T> extends LinearLayout implements View.OnClickListener {

    protected OnItemClickListener<T> onItemClickListener;

    public BaseViewHolder(Context context, View view, OnItemClickListener<T> onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
        this.addView(view);
        this.setLayoutParams(new RecyclerView.LayoutParams(
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        initUI();
        setListener();
    }

    protected abstract void initUI();

    protected abstract void setListener();

    protected abstract void bind(T t);

    @Override
    public void onClick(View v) {

    }
}
