package com.native_gallery_android.adapter.core;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;


import com.native_gallery_android.adapter.core.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseRecyclerViewAdapter<T, V extends View> extends RecyclerView.Adapter<BaseViewWrapper<V>> {

  protected List<T> items = new ArrayList<>();

  protected OnItemClickListener<T> clickListener;

  @Override
  public BaseViewWrapper<V> onCreateViewHolder(ViewGroup parent, int viewType) {
    return new BaseViewWrapper<>(onCreateItemView(parent, viewType));
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public void setItems(List<T> items) {
    if (items == null) {
      return;
    }
    this.items = items;
    this.notifyDataSetChanged();
  }


  public void setClickListener(OnItemClickListener<T> clickListener) {
    this.clickListener = clickListener;
  }

  public List<T> getItems() {
    return items;
  }

  protected abstract V onCreateItemView(ViewGroup parent, int viewType);
}
