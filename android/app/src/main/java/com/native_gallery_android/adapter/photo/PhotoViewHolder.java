package com.native_gallery_android.adapter.photo;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.native_gallery_android.R;
import com.native_gallery_android.adapter.core.BaseViewHolder;
import com.native_gallery_android.adapter.core.interfaces.OnItemClickListener;
import com.native_gallery_android.dialog.AlertMessageErrorDialog;
import com.native_gallery_android.photo.Photo;
import com.native_gallery_android.util.TypeMedia;

import java.io.File;

public class PhotoViewHolder extends BaseViewHolder<Photo> {
  private ImageView imageView;
  private Photo photo;
  private ImageView iconVideo;
  private View checkBox;
  private PhotoAdapter photoAdapter;
  private View background;

  public PhotoViewHolder(View view, OnItemClickListener<Photo> onItemClickListener, PhotoAdapter photoAdapter) {
    super(view.getContext(), view, onItemClickListener);
    this.photoAdapter = photoAdapter;
  }

  @Override
  protected void initUI() {
    imageView = findViewById(R.id.image);
    iconVideo = findViewById(R.id.icon_video);
    checkBox = findViewById(R.id.check);
    background = findViewById(R.id.background);
  }

  @Override
  protected void setListener() {
    imageView.setOnClickListener(this);
  }


  @Override
  public void onClick(View v) {
    if (photoAdapter.isSelected()) {
      if (photoAdapter.photoViewMap.size() == 4 && !photo.isCheck()) {
        AlertMessageErrorDialog.showAlertDialog(getContext());
        return;
      }
      photo.setCheck(!photo.isCheck());
      if (photo.isCheck()) {
        photoAdapter.photoViewMap.put(photo, checkBox);
      } else {
        photoAdapter.photoViewMap.remove(photo);
      }
      checkBox.setVisibility(photo.isCheck() ? VISIBLE : INVISIBLE);
      return;
    }
    if (photoAdapter.preView != null)
      photoAdapter.preView.setVisibility(INVISIBLE);
    photoAdapter.preView = background;
    photoAdapter.preView.setVisibility(VISIBLE);
    photoAdapter.photo = photo;
    if (photo.getType() == TypeMedia.PHOTO) {
      onItemClickListener.onItemClickPhoto(photo, (int) PhotoViewHolder.this.getTag());
    } else {
      onItemClickListener.onItemClickVideo(photo, (int) PhotoViewHolder.this.getTag());
    }

  }

  @Override
  protected void bind(Photo photo) {
    this.photo = photo;
    if (photo.getType() != TypeMedia.PHOTO) {
      iconVideo.setVisibility(VISIBLE);
    } else {
      iconVideo.setVisibility(GONE);
    }
    Glide.with(getContext()).load(new File(photo.getPath())).into(imageView);

    checkBox.setVisibility(photo.isCheck() ? VISIBLE : INVISIBLE);
    if (photo.isCheck()) {
      photoAdapter.photoViewMap.put(photo, checkBox);
    }
    background.setVisibility(INVISIBLE);
    if (photoAdapter.photo != null && photoAdapter.photo.equals(photo)) {
      background.setVisibility(VISIBLE);
      photoAdapter.preView = background;
    }
  }
}
