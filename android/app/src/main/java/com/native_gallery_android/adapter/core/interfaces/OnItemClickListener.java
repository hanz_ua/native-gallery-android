package com.native_gallery_android.adapter.core.interfaces;


public interface OnItemClickListener<T> {
    void onItemClickPhoto(T t, int position);

  void onItemClickVideo(T t, int position);

    void onItemClickCheck(T t, int position);
}
