package com.native_gallery_android.adapter.photo;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.native_gallery_android.R;
import com.native_gallery_android.adapter.core.BaseRecyclerViewAdapter;
import com.native_gallery_android.adapter.core.BaseViewWrapper;
import com.native_gallery_android.photo.Photo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoAdapter extends BaseRecyclerViewAdapter<Photo, PhotoViewHolder> {
  private boolean isSelected = false;
  public View preView;
  public Photo photo;
  public Map<Photo, View> photoViewMap;

  public void setSelected(boolean selected) {
    isSelected = selected;
    if (photoViewMap != null) {
      for (Photo photo : photoViewMap.keySet()) {
        photo.setCheck(false);
        photoViewMap.get(photo).setVisibility(View.INVISIBLE);
      }
    }
    photoViewMap = new HashMap<>();
  }

  public boolean isSelected() {
    return isSelected;
  }

  @Override
  protected PhotoViewHolder onCreateItemView(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(
      R.layout.item_photo, parent, false);
    return new PhotoViewHolder(view, clickListener, this);
  }

  @Override
  public void onBindViewHolder(@NonNull BaseViewWrapper<PhotoViewHolder> holder, int position) {
    PhotoViewHolder viewHolder = (PhotoViewHolder) holder.getView();
    Photo photo = getItems().get(position);
    if (photo != null) {
      viewHolder.setTag(position);
      viewHolder.bind(photo);
    }
  }
}
