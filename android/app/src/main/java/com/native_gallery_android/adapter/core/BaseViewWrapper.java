package com.native_gallery_android.adapter.core;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class BaseViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V itemView;

    public BaseViewWrapper(V itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    public View getView() {
        return itemView;
    }

}
