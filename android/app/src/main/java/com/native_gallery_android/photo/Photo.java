package com.native_gallery_android.photo;

import android.support.annotation.NonNull;

import com.native_gallery_android.util.TypeMedia;

public class Photo implements Comparable<Photo> {
  private String path;
  private TypeMedia type;
  private boolean isCheck;

  public Photo() {
  }

  public Photo(String path, TypeMedia type, boolean isCheck) {
    this.path = path;
    this.type = type;
    this.isCheck = isCheck;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public TypeMedia getType() {
    return type;
  }

  public void setType(TypeMedia type) {
    this.type = type;
  }

  public boolean isCheck() {
    return isCheck;
  }

  public void setCheck(boolean check) {
    isCheck = check;
  }

  @Override
  public int compareTo(@NonNull Photo photo) {
    return photo.getPath().compareTo(path);
  }
}
