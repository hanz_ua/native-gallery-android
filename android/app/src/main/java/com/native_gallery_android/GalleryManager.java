package com.native_gallery_android;

import android.app.Activity;
import android.widget.Toast;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.Map;

import javax.annotation.Nullable;


public class GalleryManager extends SimpleViewManager<ContainerGallery> implements LifecycleEventListener, DeviceEventManagerModule.RCTDeviceEventEmitter {
  private static final int COMMAND_HIDE = 559;
  private static final int COMMAND_SHOW = 365;
  private Activity mActivity;
  private ThemedReactContext reactContext;
  public ContainerGallery mView;

  public GalleryManager(Activity activity) {
    mActivity = activity;
  }


  @Override
  public void onHostResume() {
  }

  @Override
  public void onHostPause() {
  }

  @Override
  public void onHostDestroy() {

  }

  @Override
  public String getName() {
    return "GalleryAndroid";
  }

  @Override
  protected ContainerGallery createViewInstance(ThemedReactContext reactContext) {
    mActivity = reactContext.getCurrentActivity();
    this.reactContext = reactContext;
    reactContext.addLifecycleEventListener(this);
    mView = new ContainerGallery(mActivity, GalleryManager.this);
    return mView;
  }

  @Override
  public Map<String, Integer> getCommandsMap() {
    return MapBuilder.of(
      "hide", COMMAND_HIDE,
      "show", COMMAND_SHOW
    );
  }

  @Override
  public void receiveCommand(
    ContainerGallery view,
    int commandType,
    @Nullable ReadableArray args) {
    Assertions.assertNotNull(view);

    switch (commandType) {
      case COMMAND_SHOW:
        mView.show();
        break;
      case COMMAND_HIDE:
        mView.hide();
        break;
    }
  }


  @Override
  public void emit(String eventName, @Nullable Object data) {
  }

  public void onClose() {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit("hide", null);
  }

  public void onShow() {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit("show", null);
  }

}
